        create or replace PACKAGE BODY XXATM_SO_VALIDATIONS AS PROCEDURE process_sales_order(p_correlativo IN NUMBER, x_response OUT CLOB) AS e_error EXCEPTION;
    x_error_explanation VARCHAR2 (3200);
    l_validation_status_c varchar2(1);
    l_validation_status_d varchar2(1);
    l_x_SALIDA varchar2(3200);
    l_prod_precio number;
    l_prod_desc number;
    l_condicion_pago varchar2(3200);
    l_descuento_tope number;
    l_status varchar2(1);
    l_status_explanation varchar2(3200);

    CURSOR cabecera IS 
    SELECT
        * 
    FROM
        XXATM_NOTA_PEDIDO_CABECERA
    WHERE
        id = nvl(p_correlativo, id) 
        and validation_status <> 'Y';
        
    CURSOR detalle(p_correlativo in number) IS 
    SELECT
        npd.* 
    FROM
        XXATM_NOTA_PEDIDO_DETALLE npd 
    WHERE
        p_correlativo = npd.nota_pedido_cabecera_id;
   
    CURSOR regla IS 
    SELECT   
        XXATM_NOTA_PEDIDO_CABECERA.ID AS NPC_CORRELATIVO,
        to_number(XXATM_NOTA_PEDIDO_DETALLE.DESCUENTO_PRODUCTO) AS D_V,
        to_number(XXATM_NOTA_PEDIDO_DETALLE.PRECIO_PRODUCTO) AS P_V,
        to_number(XXATM_MAESTRO_PRODUCTOS.PROD_DESC) AS D_L,
        to_number(XXATM_MAESTRO_PRODUCTOS.PROD_PRECIO) AS P_L,
        to_number(XXATM_DESCUENTO_EMPRESA.DESCUENTO_TOPE) AS D_E 
    FROM
        XXATM_NOTA_PEDIDO_CABECERA,
        XXATM_NOTA_PEDIDO_DETALLE,
        XXATM_MAESTRO_PRODUCTOS,
        XXATM_DESCUENTO_EMPRESA 
    WHERE
        XXATM_NOTA_PEDIDO_CABECERA.ID = XXATM_NOTA_PEDIDO_DETALLE.NOTA_PEDIDO_CABECERA_ID 
        AND XXATM_MAESTRO_PRODUCTOS.ITEM_NUMBER = XXATM_NOTA_PEDIDO_DETALLE.CODIGO_PRODUCTO 
        AND XXATM_NOTA_PEDIDO_CABECERA.EMPRESA_HOLDING = XXATM_DESCUENTO_EMPRESA.CODIGO_EMPRESA 
        AND XXATM_NOTA_PEDIDO_CABECERA.ID = p_correlativo;
        
BEGIN
    x_response := null;
    IF P_CORRELATIVO is null THEN
        x_error_explanation := x_error_explanation || 'El correlativo NO puede ser nulo o vacio';
        raise e_error;
    END IF;
    
    x_response := 'EL NUMERO DE PEDIDO ES: ' || P_CORRELATIVO || '<br> <br>';
    --VALIDACIONES CABECERA
    
FOR c IN cabecera LOOP 
    BEGIN
        SELECT
            XXATM_MAESTRO_CLIENTES.CONDICION_PAGO into l_condicion_pago 
        FROM
            XXATM_MAESTRO_CLIENTES 
        WHERE
            XXATM_MAESTRO_CLIENTES.RUT_CLIENTE = c.rut_cliente;
    END;
    l_validation_status_c := 'Y';
    
    
-- VALIDACION BLOQUEO CLIENTE POR CREDITO Y COBRANZA
BEGIN
   null;
END
;
-- VERIFICACION DE PROTESTOS Y VENCIDOS
BEGIN
   null;
END
;
-- VERIFICA DEUDA VS LIMITE DE CREDITO
BEGIN
   null;
END
;
-- CUMPLIMIENTO DE CONDICIONES DE PAGO
BEGIN
   -- VERIFICA QUE CONDICION DE PAGO SEA 'DA' EN LA NOTA DE VENTA
dbms_output.put_line('CONDICIONES DE PAGO');


IF c.condicion_pago = 'DA' THEN
        
        
        IF c.condicion_pago != l_condicion_pago THEN
                -- SI LA NOTA DE VENTA VIENE POR DA O DB Y EL CLIENTE TIENE PERMITIDO PAGAR CON OTRA FORMA DE PAGO
                IF  c.condicion_pago = 'DA' 
                AND l_condicion_pago = 'CA' 
                OR  l_condicion_pago = 'CB' 
                OR  l_condicion_pago = 'CD' 
                OR  l_condicion_pago = 'CE' 
                OR  l_condicion_pago = 'CF' 
                OR  l_condicion_pago = 'CG' 
                OR  l_condicion_pago = 'CH'
                
                OR  l_condicion_pago = 'LA' 
                OR  l_condicion_pago = 'LB'
                OR  l_condicion_pago = 'LC'
                OR  l_condicion_pago = 'LD'
                OR  l_condicion_pago = 'LE'
                OR  l_condicion_pago = 'LF'
                
                OR  l_condicion_pago = 'HA'
                OR  l_condicion_pago = 'HB'
                OR  l_condicion_pago = 'HC'
                OR  l_condicion_pago = 'HD'
                OR  l_condicion_pago = 'HE'
                OR  l_condicion_pago = 'HF'
                OR  l_condicion_pago = 'HG'
                
                THEN
                        -- CLIENTE CON CONDICION DE PAGO C PAGANDO CON DA
                        l_validation_status_d :=  'Y';
                        l_status_explanation :=  'CLIENTE CON CONDICION DE PAGO C PAGANDO CON DA';
                        insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                ELSE
                        l_validation_status_d :=  'E';
                        l_status_explanation :=  'ELSE CLIENTE CON CONDICION DE PAGO C PAGANDO CON DA';
                        insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                END IF; -- END IF c.condicion_pago = 'DA'
        END IF;
                    
-- VERIFICA QUE CONDICION DE PAGO SEA 'DB' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'DB' THEN


        IF c.condicion_pago != l_condicion_pago THEN
         -- SI LA NOTA DE VENTA VIENE POR DA O DB Y EL CLIENTE TIENE PERMITIDO PAGAR CON OTRA FORMA DE PAGO
             IF c.condicion_pago = 'DB' 
                AND l_condicion_pago = 'CA' 
                OR l_condicion_pago = 'CB' 
                OR l_condicion_pago = 'CD' 
                OR l_condicion_pago = 'CE' 
                OR l_condicion_pago = 'CF' 
             
                OR  l_condicion_pago = 'CG' 
                OR  l_condicion_pago = 'CH'
               
                OR  l_condicion_pago = 'LA' 
                OR  l_condicion_pago = 'LB'
                OR  l_condicion_pago = 'LC'
                OR  l_condicion_pago = 'LD'
                OR  l_condicion_pago = 'LE'
                OR  l_condicion_pago = 'LF'
               
                OR  l_condicion_pago = 'HA'
                OR  l_condicion_pago = 'HB'
                OR  l_condicion_pago = 'HC'
                OR  l_condicion_pago = 'HD'
                OR  l_condicion_pago = 'HE'
                OR  l_condicion_pago = 'HF'
                OR  l_condicion_pago = 'HG' THEN
                    -- CLIENTE CON CONDICION DE PAGO C PAGANDO CON DB
                    l_validation_status_d :=  'Y';
                    l_status_explanation :=  'CLIENTE CON CONDICION DE PAGO C PAGANDO CON DB';
                    insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
             ELSE
                    l_validation_status_d :=  'E';
                    l_status_explanation :=  'ELSE CLIENTE CON CONDICION DE PAGO C PAGANDO CON DB';
                    insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
             END IF;
        ELSE
                l_status_explanation :=  'CONDICION DE PAGO "DB" VALIDADA CORRECTAMENTE. PEDIDO QUEDA RETENIDO A LA ESPERA DE VALIDACION DE DEPOSITO';
                l_validation_status_d:=  'E';
                insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;
-- VERIFICA QUE CONDICION DE PAGO SEA 'CA' EN LA NOTA DE VENTA        
ELSIF c.condicion_pago = 'CA' THEN
        -- VALIDACION DE MONTO CA
        IF l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN
                l_status_explanation :=  'CA-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
                l_validation_status_d:=  'E';
                insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSE
            -- ESTA VALIDACION NO TIENE MONTO
            null;
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'CB' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CB' THEN
        dbms_output.put_line('entrada CB');
        --VALIDACION DE CB
        IF l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN    
                l_status_explanation :=  'CC-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
                l_validation_status_d:=  'E';
                insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                
        ELSIF c.monto_total <= 150000 THEN
                l_status_explanation :=  'CB-CONDICION DE PAGO CB VALIDADA';
                l_validation_status_d:=  'Y';
                insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
    
        ELSIF c.monto_total > 150000 THEN
                l_status_explanation :=  'CB-MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
                l_validation_status_d:=  'Y';   
                insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'CC' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CC' THEN

        --VALIDACION DE CC
        IF l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN
        
            l_status_explanation :=  'CC-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
   
        ELSIF c.monto_total <= 300000 THEN
        
            l_status_explanation :=  'CC-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
            
        ELSIF c.monto_total > 300000 THEN
        
            l_status_explanation :=  'CC-MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'CD' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CD' THEN
        --VALIDACION DE CB
        IF l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN
            l_status_explanation :=  'CC-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        
        ELSIF c.monto_total <= 300000 THEN
            l_status_explanation :=  'CD-CONDICION DE PAGO CB VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        
        ELSIF c.monto_total > 300000 THEN
            l_status_explanation :=  'CD-MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;
    
-- VERIFICA QUE CONDICION DE PAGO SEA 'CE' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CE' THEN
        --VALIDACION DE CB
        IF l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN
            l_status_explanation :=  'CC-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
   
        ELSIF c.monto_total <= 600000 THEN
            l_status_explanation :=  'CE-CONDICION DE PAGO CB VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
            
        ELSIF c.monto_total > 600000 THEN
            l_status_explanation :=  'CE-MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
            
        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'CF' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CF' THEN
        --VALIDACION DE CB
        IF  l_condicion_pago = 'DA' OR l_condicion_pago = 'DB' THEN
            l_status_explanation :=  'CF-CLIENTE SOLO PUEDE PAGAR CON DA O DB';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        
        ELSIF c.monto_total <= 600000 THEN
            l_status_explanation :=  'CF-CONDICION DE PAGO CB VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 600000 THEN
            l_status_explanation :=  'CF-MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'CG' EN LA NOTA DE VENTA    
ELSIF c.condicion_pago = 'CG' THEN
        --VALIDACION DE CB
        IF c.monto_total <= 600000 THEN
            l_status_explanation :=  'CG-CONDICION DE PAGO CB VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 600000 THEN
            l_status_explanation :=  'CG -MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'CH' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CH' THEN
        null;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'CI' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'CI' THEN
        null;

-- LETRAS                    
-- VERIFICA QUE CONDICION DE PAGO SEA 'LA' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'LA' THEN
        IF c.monto_total <= 150000 THEN
            l_status_explanation :=  'LA-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 150000 THEN
            l_status_explanation :=  'LA -MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'LB' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'LB' THEN
        IF c.monto_total <= 300000 THEN
            l_status_explanation :=  'LB-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 300000 THEN
            l_status_explanation :=  'LB -MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'LC' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'LC' THEN
        IF c.monto_total <= 300000 THEN
            l_status_explanation :=  'LC-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 300000 THEN
            l_status_explanation :=  'LC -MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'LD' EN LA NOTA DE VENTA    
ELSIF c.condicion_pago = 'LD' THEN
        IF c.monto_total <= 600000 THEN
            l_status_explanation :=  'LD-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        ELSIF c.monto_total > 600000 THEN
            l_status_explanation :=  'LD -MONTO EXCEDE SU CONDICION DE PAGO PERMITIDA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'LE' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'LE' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'LF' EN LA NOTA DE VENTA        
ELSIF c.condicion_pago = 'LF' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'LG' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'LG' THEN
        null;

-- CHEQUE ADJUNTO
-- VERIFICA QUE CONDICION DE PAGO SEA 'HA' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'HA' THEN
        -- SI LA CONDICION DE PAGO NOTA VENTA = CONDICION PAGO CLIENTE
        IF c.condicion_pago = l_condicion_pago THEN
            l_status_explanation :=  'HA-CONDICION DE PAGO VALIDADA';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
        -- SI LA CONDICION DE PAGO NOTA VENTA ES DISTINTA A LA CONDICION DE PAGO DE CLIENTE PERO EL CLIENTE ES CTA. CORRIENTE    
        ELSIF c.condicion_pago != l_condicion_pago AND c.condicion_pago = 'CA' OR l_condicion_pago = 'CB' OR l_condicion_pago = 'CC' OR l_condicion_pago = 'CD'  OR l_condicion_pago = 'CE' OR l_condicion_pago = 'CF' THEN
            null;       
        END IF;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HB' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'HB' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HC' EN LA NOTA DE VENTA
ELSIF c.condicion_pago = 'HC' THEN
    
    IF c.condicion_pago = l_condicion_pago THEN


                -- SI EL MONTO DE LA VENTA NO EXCEDE EL MONTO PERMITIDO POR HC
                IF c.monto_total <= 150000 THEN
                    -- LA VENTA CUMPLE CON EL MONTO
                      l_status_explanation :=  'HC-CONDICION DE PAGO VALIDADA';
                      l_validation_status_d:=  'Y';
                      insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                ELSE
                    -- LA VENTA NO CUMPLE CON EL MONTO PERMITIDO
                      l_status_explanation :=  'HC-LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                      l_validation_status_d:=  'E';
                      insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                END IF;
                -- SI LA CONDICION DE PAGO NOTA VENTA ES DISTINTA A LA CONDICION DE PAGO DE CLIENTE PERO EL CLIENTE ES CTA. CORRIENTE    



ELSIF c.condicion_pago != l_condicion_pago AND c.condicion_pago = 'CA' OR l_condicion_pago = 'CB' OR l_condicion_pago = 'CC' OR l_condicion_pago = 'CD'  OR l_condicion_pago = 'CE' OR l_condicion_pago = 'CF' THEN
            -- SI LA CONDICION DE PAGO ES CA VALIDO EL MONTO DE LA NOTA CON EL MONTO


                IF l_condicion_pago = 'CA' THEN
                    null;
                ELSIF l_condicion_pago = 'CB' THEN
                        IF c.monto_total <= 150000 THEN
                            -- LA VENTA CUMPLE CON EL MONTO
                            l_status_explanation :=  'HC-CB LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HC-CB LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CC' THEN
                        IF c.monto_total <= 300000 THEN
                            l_status_explanation :=  'HC-CC LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HC-CC LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CD' THEN
                        IF c.monto_total <= 300000 THEN
                            l_status_explanation :=  'HC-CD LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HC-CD LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CE' THEN
                        IF c.monto_total <= 600000 THEN
                            l_status_explanation :=  'HC-CE LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HC-CE LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CF' THEN
                        IF c.monto_total <= 600000 THEN
                            l_status_explanation :=  'HC-CF LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HC-CF LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                END IF;
END IF;                
        
                


-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'HD' THEN
-- SI LA CONDICION DE PAGO NOTA VENTA = CONDICION PAGO CLIENTE
     
        IF c.condicion_pago = l_condicion_pago THEN
                -- SI EL MONTO DE LA VENTA NO EXCEDE EL MONTO PERMITIDO POR HC
                IF c.monto_total <= 300000 THEN
                    -- LA VENTA CUMPLE CON EL MONTO
                      l_status_explanation :=  'HD-CONDICION DE PAGO VALIDADA';
                      l_validation_status_d:=  'Y';
                      insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                ELSE
                    -- LA VENTA NO CUMPLE CON EL MONTO PERMITIDO
                      l_status_explanation :=  'HD-LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                      l_validation_status_d:=  'E';
                      insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);
                END IF;
                -- SI LA CONDICION DE PAGO NOTA VENTA ES DISTINTA A LA CONDICION DE PAGO DE CLIENTE PERO EL CLIENTE ES CTA. CORRIENTE    
         ELSIF c.condicion_pago != l_condicion_pago AND c.condicion_pago = 'CA' OR l_condicion_pago = 'CB' OR l_condicion_pago = 'CC' OR l_condicion_pago = 'CD'  OR l_condicion_pago = 'CE' OR l_condicion_pago = 'CF' THEN
            -- SI LA CONDICION DE PAGO ES CA VALIDO EL MONTO DE LA NOTA CON EL MONTO
                IF l_condicion_pago = 'CA' THEN
                    null;
                ELSIF l_condicion_pago = 'CB' THEN
                        IF c.monto_total <= 300000 THEN
                            -- LA VENTA CUMPLE CON EL MONTO
                            l_status_explanation :=  'HD-CB LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HD-CB LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CC' THEN
                        IF c.monto_total <= 300000 THEN
                            l_status_explanation :=  'HD-CC LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HD-CC LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CD' THEN
                        IF c.monto_total <= 300000 THEN
                            l_status_explanation :=  'HD-CD LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HD-CD LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CE' THEN
                        IF c.monto_total <= 600000 THEN
                            l_status_explanation :=  'HD-CE LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'HD-CE LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                ELSIF l_condicion_pago = 'CF' THEN
                        IF c.monto_total <= 600000 THEN
                            l_status_explanation :=  'HD-CF LA VENTA CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        ELSE
                            l_status_explanation :=  'H-CF LA VENTA NO CUMPLE CON EL MONTO PERMITIDO';
                            l_validation_status_d:=  'E';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
                        END IF;
                END IF;
        ELSIF c.condicion_pago != l_condicion_pago THEN
                            l_status_explanation :=  'HD NO SE CUMPLE NINGUNA CONDICION';
                            l_validation_status_d:=  'Y';
                            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            
        
        END IF;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HE' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HF' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HG' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HH' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HI' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'HD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'HJ' THEN
        -- PEDIDO QUEDA RETENIDO SIEMPRE
        null;
-- EFECTIVO O DEPOSITO
-- VERIFICA QUE CONDICION DE PAGO SEA 'WA' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'WA' THEN
        
        dbms_output.put_line('entro al WA');
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'WA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'WA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;


-- VERIFICA QUE CONDICION DE PAGO SEA 'WB' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'WB' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'WA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'WA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'XA' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'XA' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'XA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'XB' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'XB' THEN
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XB -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'XB -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
-- VERIFICA QUE CONDICION DE PAGO SEA 'XC' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'XC' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XC -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'XC -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'XD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'XD' THEN
        
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XD -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'WD -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'XE' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'XE' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XE -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'XE -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'XF' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'XF' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'XF -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'XF -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;


-- VERIFICA QUE CONDICION DE PAGO SEA 'YA' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'YA' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'YA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'YA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'YB' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'YB' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'YB -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'YB -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'YC' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'YC' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'YC -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'YC -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'YD' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'YD' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'YD -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'YD -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;

-- VERIFICA QUE CONDICION DE PAGO SEA 'ZA' EN LA NOTA DE VENTA                
ELSIF c.condicion_pago = 'ZA' THEN
        
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZB' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'ZB' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZB -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZB -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZC' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'ZC' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZC -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZC -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZD' EN LA NOTA DE VENTA                        
ELSIF c.condicion_pago = 'ZD' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZD -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZD -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZE' EN LA NOTA DE VENTA                                
ELSIF c.condicion_pago = 'ZE' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZE -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZE -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZF' EN LA NOTA DE VENTA                                        
ELSIF c.condicion_pago = 'ZF' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZA -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZA -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'ZG' EN LA NOTA DE VENTA                                        
ELSIF c.condicion_pago = 'ZG' THEN
        
        IF c.condicion_pago = l_condicion_pago THEN
        
            l_status_explanation :=  'ZG -ENTRO PAGO- VALIDACION OK';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        ELSE 

            l_status_explanation :=  'ZG -CONDICION PAGO- PEDIDO RETENIDO';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', c.id, 'CONDICION DE PAGO', l_status, l_status_explanation, l_x_SALIDA);                            

        END IF;
        
-- VERIFICA QUE CONDICION DE PAGO SEA 'CG' EN LA NOTA DE VENTA                                                
ELSIF c.condicion_pago = 'CG' THEN
        null;
-- VERIFICA QUE CONDICION DE PAGO SEA 'CC' EN LA NOTA DE VENTA                                                        
ELSIF c.condicion_pago = 'CC' THEN
        null;
END IF;-- END IF PRINCIPAL
END;-- END BEGIN

--VALIDACIONES LINEAS
FOR d in detalle(c.id) LOOP l_validation_status_d :=  'Y';
-- PL
BEGIN
   SELECT
      mp.prod_precio into l_prod_precio 
   FROM
      XXATM_MAESTRO_PRODUCTOS mp 
   WHERE
      d.codigo_producto = mp.item_number;
    EXCEPTION
        WHEN
            OTHERS 
        THEN
        x_error_explanation :=  'Buscando l_prod_precio WHEN OTHERS THEN: ' || sqlerrm;
        l_validation_status_d:=  'E';
        raise e_error;
END;
-- DL
BEGIN
   SELECT
      mp.prod_desc INTO l_prod_desc 
   FROM
      XXATM_MAESTRO_PRODUCTOS mp 
   WHERE
      d.codigo_producto = mp.item_number;
    EXCEPTION
    WHEN
        OTHERS 
    THEN
        x_error_explanation :=  'Buscando l_prod_desc WHEN OTHERS THEN: ' || sqlerrm;
        l_validation_status_d:=  'E';
        RAISE e_error;
END;
-- DE
BEGIN
        select de.descuento_tope into l_descuento_tope from xxatm_descuento_empresa de 
        where c.empresa_holding = de.codigo_empresa;
        
        EXCEPTION WHEN OTHERS THEN
        x_error_explanation :=  'Buscando l_descuento_tope WHEN OTHERS THEN: ' || sqlerrm;
        l_validation_status_d:=  'E';
        raise e_error;
END;
-- VALIDACION PRECIO
BEGIN


    IF d.precio_producto >= l_prod_precio THEN

            l_status_explanation :=  'VALIDACION DE PRECIO ACEPTADO';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_CABECERA', d.id, 'VALIDACION PRECIO', l_status, l_status_explanation, l_x_SALIDA);

            

            --PASA A VALIDACION DE DESCUENTO   
    ELSIF d.precio_producto < l_prod_precio THEN

            
            -- VERIFICAR SO PV TIENE INCORPORADO DESCUENTO

            IF l_prod_desc = 0 THEN
                    
                    -- ES PERMITIDO ESPECIFICAR UN DESCUENTO POR EL VENDEDOR
                    l_status_explanation :=  'ES PERMITIDO ESPECIFICAR UN DESCUENTO POR EL VENDEDOR';
                    l_validation_status_d:=  'Y';
                    insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION PRECIO', l_status, l_status_explanation, l_x_SALIDA);
            ELSIF l_prod_desc = 1 THEN
                    -- PRODUCTO NO ADMITE DESCUENTOS
                    
                    x_response :=  x_response || 'no admite dcto<br>';
                    l_status_explanation :=  'PRODUCTO NO ADMITE DESCUENTO';
                    l_validation_status_d:=  'Y';
                    insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION PRECIO', l_status, l_status_explanation, l_x_SALIDA);
            END IF;
    END IF;

        


    -- DETERMINACIÓN DE DT
    IF l_prod_desc = 0 THEN
            
            l_status_explanation :=  'DT = DE';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation, l_x_SALIDA);
    ELSE
            
            x_response :=  x_response || 'DT = DL<br>';
            l_status_explanation :=  'DT = DL';
            l_validation_status_d:=  'Y';
            insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation, l_x_SALIDA);
    END IF;
END;

-- VALIDACION DESCUENTO
BEGIN
   --SI DL=0 Y DV<=DE, ES CORRECTO. SI NO, ES OBJETADO          
   IF l_prod_desc = 0 AND d.descuento_producto <= l_descuento_tope OR l_prod_desc > 1 AND d.descuento_producto <= l_prod_desc OR l_prod_desc = 1 AND d.descuento_producto = 0 THEN
            x_response :=  x_response || 'primer if 1<br>';
            l_status:=  'Y';
            l_status_explanation :=  '-SI- SE CUMPLE DL=0 Y DV<=DE';
            insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation, l_x_SALIDA);
    ELSE
            x_response :=  x_response || 'primer if 2<br>';
            l_status:=  'E';
            l_status_explanation :=  '-NO- SE CUMPLE: SI DL=0 Y DV<=DE';
            l_validation_status_d:=  'E';
            insert_log_validaciones('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation, l_x_SALIDA);
    END IF;
    
    INSERT INTO xxatm_log_validaciones (TABLA, ID_TABLA, TIPO_VALIDACION, ESTADO_VALIDACION, VALIDACION_DESCRIPCION ) 
            VALUES('NOTA_PEDIDO_DETALLE', d.id, 'VALIDACION DESCUENTO', l_status, l_status_explanation);
    
    UPDATE XXATM_NOTA_PEDIDO_DETALLE SET validation_status = l_validation_status_d WHERE d.id = id;
    commit;
END;
    -- VALIDACION DESCUENTO
    IF l_validation_status_c = 'E' THEN 
        null;
    ELSE
        l_validation_status_c :=  l_validation_status_d;
    END IF;
END LOOP; --DETALLE

    UPDATE XXATM_NOTA_PEDIDO_CABECERA
        SET validation_status = l_validation_status_c 
        WHERE c.id = id;
        COMMIT;
        
END LOOP; --CABECERA
    EXCEPTION WHEN
        e_error 
    THEN
        x_response :=  'WHEN e_error THEN' || x_error_explanation;
WHEN
   OTHERS 
THEN
   x_response :=  'WHEN OTHERS THEN: ' || sqlerrm;

END process_sales_order;

    PROCEDURE insert_log_validaciones( p_TABLA IN VARCHAR2, p_ID_TABLA IN NUMBER, p_TIPO_VALIDACION IN VARCHAR2, p_ESTADO_VALIDACION IN VARCHAR2, p_VALIDACION_DESCRIPCION IN VARCHAR2, x_SALIDA OUT VARCHAR2 ) AS 
        BEGIN
            INSERT INTO xxatm_log_validaciones ( TABLA, ID_TABLA, TIPO_VALIDACION, ESTADO_VALIDACION, VALIDACION_DESCRIPCION ) 
            VALUES(p_TABLA, p_ID_TABLA, p_TIPO_VALIDACION, p_ESTADO_VALIDACION, p_VALIDACION_DESCRIPCION );
        END insert_log_validaciones;

END XXATM_SO_VALIDATIONS;