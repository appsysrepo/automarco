create or replace procedure xxatm_wms_fact_ar is
	cursor c1 is
		select ID
				,'NEW-IMPORTADORA AUTOMARCO LIMITADA' business_unit_name
				,'IAM_WMS_FACT' TransactionSource
				,'IAM_WMS_FACT_AFE_ELE' TransactionType
				,to_char(sysdate,'YYYYMMDDHH24MISS') TrxNumber
				,to_char(sysdate,'YYYY-MM-DD') TrxDate
				,xnpc.RUT_CLIENTE BillToAccountNumber
				,'Contado' PaymentTermsName
				,'CLP' InvoiceCurrencyCode
		from XXATM_NOTA_PEDIDO_CABECERA xnpc
		where 1 = 1
        and xnpc.id = 22
		;
	cursor c2(p_NUMERO_PEDIDO_CABECERA_ID in number) is
		select rownum LineNumber
				,xnpd.CODIGO_PRODUCTO Description
				,xdd.CANTIDAD_PICKING Quantity
				,xnpd.PRECIO_PRODUCTO UnitSellingPrice
				,'IAM Venta AF' MemoLineName
		from XXATM_DETALLE_DESPACHO xdd
			,XXATM_NOTA_PEDIDO_DETALLE xnpd
		where 1 = 1
		and xdd.NUMERO_PEDIDO_CABECERA_ID = p_NUMERO_PEDIDO_CABECERA_ID
		and xnpd.NOTA_PEDIDO_CABECERA_ID = xdd.NUMERO_PEDIDO_CABECERA_ID
		;
	l_clob_message      CLOB;
	l_clob_response     CLOB;
    --TEST
    vg_wallet1               VARCHAR2 (240) := 'file:/home/oracle/certificados_erp/test';
    vg_wallet2               VARCHAR2 (240) := 'AutoMarco#2019$';
    vg_cloud_user            VARCHAR2 (240) := 'appsys';
    vg_cloud_user_password   VARCHAR2 (240) := 'Cloud2019';
                                                
    vg_receipt_service_url   VARCHAR2 (240) := 'https://ekth-test.fa.us6.oraclecloud.com:443/fscmService/RecInvoiceService';
    vg_receipt_method        VARCHAR2 (240) := 'http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/';
    vg_Content_Type          VARCHAR2 (240) := 'text/xml;charset=ISO-8859-1';
    VG_response_status_code  VARCHAR2 (240);

    PROCEDURE output (p_message IN VARCHAR2)
    IS
    BEGIN
        DBMS_OUTPUT.put_line (p_message);
    END output;

    PROCEDURE call_webservice (p_service_url   IN     VARCHAR2,
                               p_method        IN     VARCHAR2,
                               p_message       IN     CLOB,
                               x_response         OUT CLOB)
    IS
        l_http_request    UTL_HTTP.req;
        l_http_response   UTL_HTTP.resp;
        l_text            VARCHAR2 (32767);
        l_raw             RAW (32767);
        vl_xml_entrada    XMLTYPE;
        l_length          NUMBER;
        l_position        NUMBER;
        l_line_text       VARCHAR2 (32767);
        l_blob            BLOB;
        
    BEGIN
        BEGIN

            l_length := DBMS_LOB.getlength (p_message);

            vl_xml_entrada := xmltype.createxml (p_message);
            UTL_HTTP.set_wallet (vg_wallet1, vg_wallet2);
            l_http_request := UTL_HTTP.begin_request (p_service_url, 'POST', 'HTTP/1.1');
            UTL_HTTP.set_authentication (l_http_request, vg_cloud_user, vg_cloud_user_password);
            UTL_HTTP.set_header (l_http_request, 'Content-Type', vg_Content_Type);
            UTL_HTTP.set_header (l_http_request, 'Content-Length', l_length);
            UTL_HTTP.set_header (l_http_request, 'SOAPAction', p_method);
            UTL_HTTP.set_header (l_http_request, 'Accept-Encoding', 'deflate');

            l_length := 0;
            l_position := 1;
            l_line_text := NULL;

            LOOP
                l_line_text := SUBSTR (p_message, l_position, 240);
                EXIT WHEN l_line_text IS NULL;
                UTL_HTTP.write_text (l_http_request, l_line_text);
                l_position := l_position + LENGTH (l_line_text);
            END LOOP;

            l_http_response := UTL_HTTP.get_response (l_http_request);
            -- asigna el codigo de la respuesta del webservice,, si es 200 es exitoso
            VG_response_status_code := l_http_response.status_code;

            DBMS_LOB.createtemporary (l_blob, TRUE);
            BEGIN
                LOOP
                    UTL_HTTP.read_text (l_http_response, l_text, 32766);
                    x_response := x_response || l_text;
                END LOOP;
            EXCEPTION
                WHEN UTL_HTTP.end_of_body
                THEN
                    UTL_HTTP.end_response (l_http_response);
            END;
            IF l_http_response.status_code = 500
            THEN
                --x_response := NULL;
                null;
            END IF;

            IF l_http_request.private_hndl IS NOT NULL
            THEN
                UTL_HTTP.end_request (l_http_request);
            END IF;

            IF l_http_response.private_hndl IS NOT NULL
            THEN
                UTL_HTTP.end_response (l_http_response);
            END IF;

        EXCEPTION
            WHEN OTHERS
            THEN
                UTL_HTTP.end_response (l_http_response);                
                raise_application_error(-20001,'SQLERR: ' || SQLCODE || ' - ' || SQLERRM
                              ||' BACK: ' ||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        END;
    END;
begin
    vg_receipt_method       := 'http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService';
	for l1 in c1 loop
        l_clob_message  := '<soapenv:Envelope xmlns:inv="http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tran="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/" xmlns:tran1="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/" xmlns:tran2="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionInterfaceHeaderDff/" xmlns:tran3="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionInterfaceLineDff/" xmlns:tran4="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/" xmlns:tran5="http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/" xmlns:typ="http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/types/">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <typ:createSimpleInvoice>
                                 <typ:invoiceHeaderInformation>
                                    <inv:BusinessUnit>'||l1.business_unit_name||'</inv:BusinessUnit>
                                    <inv:TransactionSource>'||l1.TransactionSource||'</inv:TransactionSource>
                                    <inv:TransactionType>'||l1.TransactionType||'</inv:TransactionType>
                                    <inv:TrxNumber>'||l1.TrxNumber||'</inv:TrxNumber>
                                    <inv:TrxDate>'||l1.TrxDate||'</inv:TrxDate>
                                    <inv:BillToAccountNumber>'||l1.BillToAccountNumber||'</inv:BillToAccountNumber>
                                    <inv:PaymentTermsName>'||l1.PaymentTermsName||'</inv:PaymentTermsName>
                                    <inv:InvoiceCurrencyCode>'||l1.InvoiceCurrencyCode||'</inv:InvoiceCurrencyCode>
                                    <inv:ConversionRateType>User</inv:ConversionRateType>
                                    <inv:ConversionDate/>
                                    <inv:ConversionRate></inv:ConversionRate>';
		for l2 in c2(l1.ID) loop
            l_clob_message  := l_clob_message||'<inv:InvoiceLine>
                               <inv:LineNumber>'||l2.LineNumber||'</inv:LineNumber>
                               <inv:Description>'||l2.DESCRIPTION||'</inv:Description>
                               <inv:Quantity unitCode="">'||l2.Quantity||'</inv:Quantity>
                               <inv:UnitSellingPrice currencyCode="'||l1.InvoiceCurrencyCode||'">'||l2.UnitSellingPrice||'</inv:UnitSellingPrice>
                               <inv:MemoLineName>'||l2.MemoLineName||'</inv:MemoLineName>
                               <inv:TaxClassificationCode></inv:TaxClassificationCode>
                            </inv:InvoiceLine>';
		end loop;
        l_clob_message  := l_clob_message||'</typ:invoiceHeaderInformation>
                              </typ:createSimpleInvoice>
                           </soapenv:Body>
                        </soapenv:Envelope>';
	end loop;
    output(l_clob_message);
    call_webservice (vg_receipt_service_url,
                    vg_receipt_method,
                    l_clob_message,
                    l_clob_response);
    output(l_clob_response);
end;